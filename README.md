# Machine Learning with Python
## 1. Machine Learning Tutorial
### 1.1 Định nghĩa Machine Learning
Là một ngành nghiên cứu để giúp máy tính có **khả năng học** để giải quyết vấn đề mà không cần phải lập trình trước cho nó.

## 2. Linear Regression
### 2.1 Định nghĩa 
* Mục tiêu;
- Dự đoán chiều giá trị của 1 hoặc nhiều biến.
### 2.2 Đặc điểm khi sử dụng
* Ưu điểm: 
```
- Là thuật toán cơ bản nhất và đơn giản nhất trong machine learning, chính vì vậy nó dễ tìm hiểu nhất và thường là căn bản để mỗi người học machine learning đều tìm hiểu đầu tiên.
- Dễ hiểu -> dễ tinh chỉnh 
```
* Nhược điểm: 
```
- Vì là thuật toán đơn giản nên khó có thể theo kịp những bài toán có số lượng dữ liệu lớn và phức tạp như hiện nay
- Dễ bị bias bởi những dữ liệu abnormal -> Khi sử dụng cần dùng công nghệ để loại bỏ abnormal dữ liệu
```
## 3. Understanding Logistic Regression
### 3.1 Định Nghĩa
- Mục đích: dự đoán đầu ra là các nhãn khác nhau của một bài toán
### 3.2 Đặc điểm khi sử dụng trong bài toán tính điểm tín dụng của khách hàng
* [Ưu điểm](https://tuanvanle.wordpress.com/2013/06/08/mo-hinh-logistic-trong-xep-hang-rui-ro-tin-dung/)
```
- Do mô hình này cũng là mô hình toán học nên có những ưu điểm giống như mô hình điểm số Z. Do đây là mô hình định lượng nên khắc phục được những nhược điểm của mô hình định tính, thể hiện sự khách quan, nhất quán, không phụ thuộc vào ý kiến chủ quan của cán bộ tín dụng.
- Mô hình Logistic này có kỹ thuật đo lường rủi ro tín dụng khá đơn giản, dễ thực hiện bằng phần mềm chuyên dụng (như Eviews). Đây là lợi thế nếu so với mô hình KMV có kỹ thuật đo lường và các bước tính toán khá phức tạp.
- Mô hình Logistic có thể là cơ sở để ngân hàng phân loại khách hàng và nhận riện rủi ro. Thông qua kết quả từ mô hình, chúng ta có thể ước lượng được xác suất không trả được nợ của khách hàng, từ đó Ngân hàng có thể xác định được doanh nghiệp nào đang nằm trong vùng an toàn, doanh nghiệp nào nằm trong vùng cảnh báo và giúpngân hàng chủ động trong việc đưa ra những biện pháp hạn chế rủi ro.
– Một ưu điểm nổi bật của mô hình Logistic so với mô hình xếp hạng tín dụng truyền thống hay mô hình KMV, đó là mô hình Logistic có thể đo lường vai trò của các yếu tố tác động đến hạng tín dụng của khách hàng. Ngoài ra, trong khi mô hình điểm số Z lại cứng nhắc trong việc xem xét các yếu tố tác động tới biến phụ thuộc và các hệ số của chúng (do Altman đưa ra), trong khi với mô hình Logistic chúng ta có thể dễ dàng hiệu chỉnh hoặc thêm bớt các biến nhằm xác định cụ thể tác động của các yếu tố tới rủi ro tín dụng là như thế nào.
```
 
* Nhược điểm
```
– Mô hình Logistic vẫn tồn tại nhược điểm, đó là mô hình phụ thuộc vào mức độ chính xác của nguồn thông tin thu nhập và khả năng dự báo cũng như trình độ phân tích của cán bộ tín dụng. Ngoài ra, mô hình Logistic bản chất là mô hình kinh tế lượng, vì vậy khi hệ số xác định ở mức nhỏ thì mô hình có thể dự báo kém chính xác (thể hiện qua các giá trị phần dư).
```

## 4. K means Clustering
## 5. Python | Image Classification using keras
## 6. creating a simple machine learning model
## 7. Python | Implementation of Movie Recommender System
## 8. ML | Boston Housing Kaggle Challenge with Linear Regression
## 9. Cancer cell classification using Scikit-learn
## 10. Saving a machine learning Model
## 11. Applying Convolutional Neural Network on mnist dataset
## 12. Python | NLP analysis of Restaurant reviews
## 13. Learning Model Building in Scikit-learn
## 14. Implementing Artificial Neural Network training process
## 15. A single neuron neural network in Python
## 16. Python | How and where to apply Feature Scaling?
## 17. Identifying handwritten digits using Logistic Regression in PyTorch

References:
1. [Machine Learning](https://www.geeksforgeeks.org/machine-learning/#dp)