import cv2
import numpy as np


def resize_image(input_path, output_path, percent):
    try:
        # Read image from disk.
        img = cv2.imread(input_path)

        # Get number of pixel horizontally and vertically.
        (height, width) = img.shape[:2]

        # Specify the size of image along with interploation methods.
        # cv2.INTER_AREA is used for shrinking, whereas cv2.INTER_CUBIC
        # is used for zooming.
        res = cv2.resize(img, (int(width * percent), int(height * percent)), interpolation=cv2.INTER_CUBIC)

        # Write image back to disk.
        cv2.imwrite(output_path, res)

    except IOError:
        print('Error while reading files !!!')

def rotate_image(input_path, output_path, rotage_degree):
    try:
        # Read image from the disk.
        img = cv2.imread(input_path)

        # Shape of image in terms of pixels.
        (rows, cols) = img.shape[:2]

        # getRotationMatrix2D creates a matrix needed for transformation.
        # We want matrix for rotation w.r.t center to 45 degree without scaling.
        M = cv2.getRotationMatrix2D((cols / 2, rows / 2), rotage_degree, 1)
        res = cv2.warpAffine(img, M, (cols, rows))

        # Write image back to disk.
        cv2.imwrite(output_path, res)
    except IOError:
        print('Error while reading files !!!')

def edge_detection(input_path, output_path, x, y):
    try:
        # Read image from disk.
        img = cv2.imread(input_path)

        # Canny edge detection.
        edges = cv2.Canny(img, x, y)

        # Write image back to disk.
        cv2.imwrite(output_path, edges)
    except IOError:
        print('Error while reading files !!!')
